import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPageComponent } from 'src/app/main-page/main-page.component';
import { CategoryListComponent } from 'src/app/category-list/category-list.component';
import { ItemDetailsComponent } from 'src/app/item-details/item-details.component';
import { OrderSummaryComponent } from 'src/app/order-summary/order-summary.component';
import { OrderPlacedComponent } from 'src/app/order-placed/order-placed.component';
import { OrderConfirmedComponent } from 'src/app/order-confirmed/order-confirmed.component';

const routes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: MainPageComponent },
  { path: 'category-list', component: CategoryListComponent },
  { path: 'item-details', component: ItemDetailsComponent },
  { path: 'order-summary', component: OrderSummaryComponent },
  { path: 'order-placed', component: OrderPlacedComponent },
  { path: 'order-confirmed', component: OrderConfirmedComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
