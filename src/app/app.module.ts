import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';
import { OrderPlacedComponent } from './order-placed/order-placed.component';
import { OrderConfirmedComponent } from './order-confirmed/order-confirmed.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    CategoryListComponent,
    ItemDetailsComponent,
    OrderSummaryComponent,
    OrderPlacedComponent,
    OrderConfirmedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // FORMS
    FormsModule,
    ReactiveFormsModule,
    // MATERIAL
    MatCardModule,
    // HTTP
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
