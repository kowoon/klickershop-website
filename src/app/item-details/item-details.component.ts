import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {

  inputForm: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.form();
  }

  form() {
    this.inputForm = new FormGroup({
      inputValue: new FormControl({value: 1, disabled: true}),
    });
  }

  addOne() {
    document.getElementsByClassName('btn-minus')[0].classList.remove('disabled');
    this.inputForm.patchValue({
      inputValue: (Number(this.inputForm.value.inputValue) + 1).toString()
    });
  }

  minusOne() {
    if (Number(this.inputForm.value.inputValue) === 1) {
      document.getElementsByClassName('btn-minus')[0].classList.add('disabled');
    }
    if (Number(this.inputForm.value.inputValue) > 0) {
      this.inputForm.patchValue({
        inputValue: (Number(this.inputForm.value.inputValue) - 1).toString()
      });
    }
  }
}
