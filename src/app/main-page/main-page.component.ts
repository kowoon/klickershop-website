import { Component, OnInit } from '@angular/core';

import { ApiOrganizerD1ShopService } from 'src/services/api/organizer/D1_Shop/d1-shop.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  expandProductCategory = false;

  constructor(
    private apiOrganizerD1ShopService: ApiOrganizerD1ShopService,
  ) { }

  ngOnInit(): void {
    this.apiOrganizerD1ShopGetList();
  }

  // API SHOP GET LIST
  apiOrganizerD1ShopGetList() {
    this.apiOrganizerD1ShopService.getList({value: ''}).subscribe(success => {
      if (success.StatusCode === 200) {
        this.apiOrganizerD1ShopListCategory(success.Result.ShopId)
        this.apiOrganizerD1ShopListFeatures(success.Result.ShopId)
        this.apiOrganizerD1ShopListProduct(success.Result.ShopId)
        console.log(success.Result);
      }
    });
  }

  // API SHOP LIST CATEGORY
  apiOrganizerD1ShopListCategory(shopId: number) {
    this.apiOrganizerD1ShopService.postListCategory(shopId).subscribe(success => {
      if (success.StatusCode === 200) {
        console.log(success.Result);
      }
    });
  }

  // API SHOP LIST FEATURES
  apiOrganizerD1ShopListFeatures(shopId: number) {
    this.apiOrganizerD1ShopService.postListFeatures(shopId).subscribe(success => {
      if (success.StatusCode === 200) {
        console.log(success.Result);
      }
    });
  }

  // API SHOP LIST PRODUCT
  apiOrganizerD1ShopListProduct(shopId: number) {
    this.apiOrganizerD1ShopService.postListProduct(shopId).subscribe(success => {
      console.log(success)
      if (success.StatusCode === 200) {
        console.log(success.Result);
      }
    });
  }

  myFunction() {
    const dots = document.getElementById('dots');
    const moreText = document.getElementById('more');
    const btnText = document.getElementById('myBtn');

    if (dots.style.display === 'none') {
        dots.style.display = 'inline';
        btnText.innerHTML = 'more';
        moreText.style.display = 'none';
    } else {
        dots.style.display = 'none';
        btnText.innerHTML = 'less';
        moreText.style.display = 'inline';
    }
  }

  expandProductCategoryFunction() {
    document.getElementsByClassName('more-products')[0].classList.toggle('show');
    if (document.getElementsByClassName('moreless-button-1')[0].textContent === 'see more') {
      document.getElementsByClassName('moreless-button-1')[0].textContent = 'see less';
    } else {
      document.getElementsByClassName('moreless-button-1')[0].textContent = 'see more';
    }
  }

  expandFeaturedProductFunction() {
    document.getElementsByClassName('more-items')[0].classList.toggle('show-card');
    if (document.getElementsByClassName('moreless-button-2')[0].textContent === 'see more') {
      document.getElementsByClassName('moreless-button-2')[0].textContent = 'see less';
    } else {
      document.getElementsByClassName('moreless-button-2')[0].textContent = 'see more';
    }
  }
}
