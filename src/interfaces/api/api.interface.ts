export interface ApiResponse<T> {
    StatusCode: number;
    ErrMsg: string;
    SuccessMsg: string;
    AlertMsg: string;
    Result: T;
}