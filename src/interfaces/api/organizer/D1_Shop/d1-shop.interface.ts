////////////////////////////// GetList //////////////////////////////
export interface GetListInput {
    value: string;
}

export interface GetListOutput {
    ShopId: number;
    ShopCode: string;
    ShopName: string;
    Banner: string;
    Info: string;
    EnableCategory: boolean;
    EnableFeatures: boolean;
    WhatsappNumber: string;
    LocationName: string;
    LocationAddress: string;
    LocationX: string;
    LocationY: string;
    FacebookLink: string;
    InstaLink: string;
    Email: string;
    Website: string;
}

export type ListCategoryInput = number;

export interface ListCategoryOutput {}

export type ListFeaturesInput = number;

export interface ListFeaturesOutput {}

export type ListProductInput = number;

export interface ListProductOutput {}

export type GetProductInput = number;

export interface GetProductOutput {}