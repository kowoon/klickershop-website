// ANGULAR
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// RXJS
import { Observable } from 'rxjs';

// INTERFACE API
import {
  // GET LIST
  GetListInput,
  GetListOutput,
  // LIST CATEGORY
  ListCategoryInput,
  ListCategoryOutput,
  // LIST FEATURES
  ListFeaturesInput,
  ListFeaturesOutput,
  // LIST PRODUCT
  ListProductInput,
  ListProductOutput,
  // GET PRODUCT
  GetProductInput,
  GetProductOutput,
} from 'src/interfaces/api/organizer/D1_Shop/d1-shop.interface';

// INTERFACE API
import { ApiResponse } from 'src/interfaces/api/api.interface';

// ENVIRONMENT
import { environment } from 'src/environments/environment';

// API URL
const apiUrl = environment.apiUrl + 'organizer/D1_Shop/';

@Injectable({
  providedIn: 'root'
})
export class ApiOrganizerD1ShopService {

  // CONSTRUCTOR
  constructor(
    // ANGULAR COMMON HTTP
    private httpClient: HttpClient
  ) { }

  // POST GET LIST
  getList(input: GetListInput): Observable<ApiResponse<GetListOutput>> {
    return this.httpClient.post<ApiResponse<GetListOutput>>(apiUrl + 'Get', input);
  }

  // POST LIST CATEGORY
  postListCategory(input: ListCategoryInput): Observable<ApiResponse<ListCategoryOutput>> {
    return this.httpClient.post<ApiResponse<ListCategoryOutput>>(apiUrl + 'ListCategory', input);
  }

  // POST LIST FEATURES
  postListFeatures(input: ListFeaturesInput): Observable<ApiResponse<ListFeaturesOutput>> {
    return this.httpClient.post<ApiResponse<ListFeaturesOutput>>(apiUrl + 'ListFeatures', input);
  }

  // POST LIST PRODUCT
  postListProduct(input: ListProductInput): Observable<ApiResponse<ListProductOutput>> {
    return this.httpClient.post<ApiResponse<ListProductOutput>>(apiUrl + 'ListProduct', input);
  }

  // GET PRODUCT
  getProduct(input: GetProductInput): Observable<ApiResponse<GetProductOutput>> {
    return this.httpClient.post<ApiResponse<GetProductOutput>>(apiUrl + 'GetProduct', input);
  }
}
